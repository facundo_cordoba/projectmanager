USE [AdministradorDeProyectos]
GO

/****** Object:  Table [dbo].[Skill]    Script Date: 11/09/2019 12:25:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Skill](
	[IdSkill] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [varchar](75) NOT NULL,
	[Grados] [int] NOT NULL,
	[Programador_ID] [int] NULL,
	[Equipo_ID] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[IdSkill] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


