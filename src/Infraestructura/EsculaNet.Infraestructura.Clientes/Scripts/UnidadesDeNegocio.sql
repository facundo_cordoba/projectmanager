USE [AdministradorDeProyectos1]
GO

/****** Object:  Table [dbo].[UnidadesDeNegocio]    Script Date: 9/9/2019 11:00:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[UnidadesDeNegocio](
	[IDUnidadDeNegocio] [int] IDENTITY(1,1) NOT NULL,
	[RazonSocial] [varchar](128) NOT NULL,
	[ResponsableDeUnidad] [varchar](64) NOT NULL,
	[Cuit] [varchar](11) NOT NULL,
	[EmailResponsable] [varchar](255) NOT NULL,
	[TelefonoResponsable] [varchar](20) NOT NULL,
	[IDCliente] [int] NOT NULL,
 CONSTRAINT [PK_UnidadesDeNegocio] PRIMARY KEY CLUSTERED 
(
	[IDUnidadDeNegocio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


