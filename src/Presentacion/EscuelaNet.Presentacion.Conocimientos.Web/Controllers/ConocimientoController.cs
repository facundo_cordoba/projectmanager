﻿using EscuelaNet.Presentacion.Conocimientos.Web.Infraestructura;
using EscuelaNet.Presentacion.Conocimientos.Web.Models;
using EscuelaNet.Dominio.Conocimientos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Conocimientos.Web.Controllers
{
    public class ConocimientoController : Controller
    { 
        public ActionResult Index(int id)
        {
            var categoria = Contexto.Instancia.Categorias[id];
            var model = new ConocimientoIndexModel()
            {
                Nombre = string.Format("Categoria {1} - Conocimiento de {0}", categoria.Nombre, categoria.Descripcion),
                Conocimientos = (List<Conocimiento>)categoria.Conocimientos,
                IdCategoria = id

            };
        return View(model);
        }

        //public ActionResult Index(int id) {
        //    var equipo = Contexto.Instancia.Equipos[id];
        //    var model = new ProgramadoresIndexModel()
        //    {
        //        Titulo = string.Format("Equipo {1} - Programadores de {0}", equipo.Pais, equipo.Nombre),
        //        Programadores = (List<Programador>)equipo.Programadores,
        //        idEquipo = id }; return View(model);
        //}


        public ActionResult New(int id)
    {
            var categoria = Contexto.Instancia.Categorias[id];
            var model = new NuevaConocimientoModel()
            {
                Titulo = "Nuevo Conocimiento para la Categoria '"+categoria.Nombre+"'", 
                IdCate = id

            };
            return View(model);
    }

    [HttpPost]
    public ActionResult New(NuevaConocimientoModel model)
    {
        if (!string.IsNullOrEmpty(model.Nombre))
        {
                try
                {
                    var idCono = model.IdConocimiento;
                    Contexto.Instancia.Categorias[model.IdCate].AgregarConocimiento(new Conocimiento(model.Nombre));

                    TempData["success"] = "Nuevo conocimiento Creado";
                    return RedirectToAction("Index/"+model.IdCate);
                }
                catch (Exception ex)
                {
                TempData["error"] = ex.Message;
                return View(model);
                }
        }
        else
        {
            TempData["error"] = "Texto vacío";
            return View(model);

        }
    }


    public ActionResult Edit(int idCategoria, int idConocimiento)
        {
            var cantCategoria = Contexto.Instancia.Categorias.Count();

            if (idCategoria < 0 || idCategoria >= cantCategoria || idConocimiento <0)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("Index");
            }
            else
            {
                var categoria = Contexto.Instancia.Categorias[idCategoria];
                var conocimiento = categoria.Conocimientos[idConocimiento];

                var model = new NuevaConocimientoModel()
                {
                    IdCate = idCategoria,
                    IdConocimiento = idConocimiento,
                    Nombre = conocimiento.Nombre,
                    Demanda = conocimiento.Demanda,
                                       
                };

                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Edit(NuevaConocimientoModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    Contexto.Instancia.Categorias[model.IdCate].Conocimientos[model.IdConocimiento].Nombre = model.Nombre;
                    Contexto.Instancia.Categorias[model.IdCate].Conocimientos[model.IdConocimiento].Demanda = model.Demanda;
                    TempData["success"] = "Conocimiento editado";
                    return RedirectToAction("Index/"+model.IdCate);
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacío";
                return View(model);
            }
        }


        public ActionResult Delete(int idCategoria, int idConocimiento)
        {
            var cantidad = Contexto.Instancia.Categorias.Count();
            if (idCategoria < 0 || idCategoria >= cantidad || idConocimiento < 0)
            {
                TempData["error"] = "Id no válido";
                return RedirectToAction("Index");
            }
            else
            {
                var categoria = Contexto.Instancia.Categorias[idCategoria];
                var conocimiento = categoria.Conocimientos[idConocimiento];

                var model = new NuevaConocimientoModel()
                {
                    Titulo = "Algo",
                    IdCate = idCategoria,
                    IdConocimiento = idConocimiento,
                    Nombre = conocimiento.Nombre,
                    Demanda = conocimiento.Demanda
                };
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Delete(NuevaConocimientoModel model)
        {

            try
            {
                Contexto.Instancia.Categorias[model.IdCate].Conocimientos.RemoveAt(model.IdConocimiento);
                TempData["success"] = "Conocimiento Borrado";
                return RedirectToAction("../Conocimiento/Index/" + model.IdCate);
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }

        }


        //public ActionResult Delete(int id)
        //{
        //    var categoria = Contexto.Instancia.Conocimientos[id];
        //    var model = new NuevaConocimientoModel()
        //    {
        //        Nombre = categoria.Nombre,
        //        Id = id
        //    };
        //    return View(model);

        //}
        //[HttpPost]
        //public ActionResult Delete(NuevaConocimientoModel model)
        //{
        //    try
        //    {
        //        Contexto.Instancia.Conocimientos.RemoveAt(model.Id);
        //        TempData["success"] = "Conocimiento borrado";
        //        return RedirectToAction("Index");
        //    }
        //    catch (Exception ex)
        //    {
        //        TempData["error"] = ex.Message;
        //        return View(model);
        //    }
        //}
    }
    


    }
