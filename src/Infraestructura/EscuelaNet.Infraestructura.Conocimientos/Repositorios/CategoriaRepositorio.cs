﻿using EscuelaNet.Dominio.Conocimientos;
using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Infraestructura.Conocimientos.Repositorios
{
    public class CategoriaRepositorio : ICategoriaRepository
    {
        private ConocimintosContext _context = new ConocimintosContext();
        public IUnitOfWork UnitOfWork => _context;

        public Categoria Add(Categoria categoria)
        {
            _context.Categorias.Add(categoria);
            return categoria;
        }

        public void Delete(Categoria categoria)
        {
            _context.Categorias.Remove(categoria);
        }

        public Categoria GetCategoria(int id)
        {
            var categoria = _context.Categorias.Find(id);
            return categoria;
        }

        public List<Categoria> ListCategoria()
        {
            return _context.Categorias.ToList();
        }

        public void Update(Categoria categoria)
        {
            _context.Entry(categoria).State = EntityState.Modified;
        }
    }
}
