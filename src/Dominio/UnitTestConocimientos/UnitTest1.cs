﻿using System;
using EscuelaNet.Dominio.Conocimientos;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestConocimientos
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void PROBAR_INGRESAR_PRECIO_NEGATIVO()
        {
            var precio = new Precio();
            Assert.AreEqual(false,precio.ValidarValorNominal(-1));
        }
    }
}
