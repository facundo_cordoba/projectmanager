﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace EscuelaNet.Dominio.Capacitaciones
{
    public class Instructor : Entity, IAggregateRoot
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Dni { get; set; }
        public IList<Tema> Temas { get; set; }
        [DisplayName("Fecha de Nacimiento")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime FechaNacimiento { get; set; }
        private Instructor()
        {

        }

        public Instructor(string nombre, string apellido, string dni, DateTime fechaNacimiento) : this()
        {
            this.Nombre = nombre ?? throw new System.ArgumentNullException(nameof(nombre));
            this.Apellido = apellido ?? throw new System.ArgumentNullException(nameof(nombre));
            this.Dni = dni ?? throw new System.ArgumentNullException(nameof(nombre));
            this.FechaNacimiento = fechaNacimiento;

        }
        public void pushTema(Tema tema)
        {
            if (this.Temas == null)
            {
                this.Temas = new List<Tema>();
            }
            if (!this.Temas.Contains(tema))
            {
                this.Temas.Add(tema);
            }
            else
                throw new Exception("Ya se encuentra el tema vinculado al Instructor.");
        }


        public void pullTema(Tema tema)
        {
            if (this.Temas != null)
            {
                if (this.Temas.Contains(tema))
                {
                    this.Temas.Remove(tema);
                }
            }
        }
    }
}