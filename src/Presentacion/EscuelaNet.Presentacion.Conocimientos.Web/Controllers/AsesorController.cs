﻿using EscuelaNet.Presentacion.Conocimientos.Web.Infraestructura;
using EscuelaNet.Presentacion.Conocimientos.Web.Models;
using EscuelaNet.Dominio.Conocimientos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Categorias.Web.Controllers
{
    public class AsesorController : Controller
    { 
        public ActionResult Index(int idCategoria, int idConocimiento)
        {
            var categoria = Contexto.Instancia.Categorias[idCategoria];
            var conocimiento = categoria.Conocimientos[idConocimiento];
            
                if (conocimiento.Asesores == null)
                {
                    TempData["error"] = "Conocimiento sin Asesores";
                    return RedirectToAction("../conocimiento/Index/"+idCategoria);
                }
            var asesores = conocimiento.Asesores;

            List<Asesor> aseso = new List<Asesor>();

            foreach (var asesor in asesores)
            {
                aseso.Add(asesor);
            }
            var model = new AsesorIndexModel()
            {
                Titulo = "Asesores del Conocimiento '" + conocimiento.Nombre + "' de la Categoría '" + conocimiento.Nombre + "'",
                IdCategoria = idCategoria,
                IdCono = idConocimiento,
                Asesores = aseso
            };

            return View(model);
        }

        public ActionResult New(int idCategoria, int idConocimiento)
        {
            var categoria = Contexto.Instancia.Categorias[idCategoria];
            var conocimiento = categoria.Conocimientos[idConocimiento];
            var model = new NuevoAsesorModel()
            {
                Titulo = "Nuevo Asesor para el Conocimiento '" + conocimiento.Nombre + "' de la Categoría '" + categoria.Nombre + "'",
                IdCate = idCategoria,
                IdConocimiento = idConocimiento
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult New(NuevoAsesorModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    Contexto.Instancia.Categorias[model.IdCate].Conocimientos[model.IdConocimiento].AgregarAsesor(new Asesor(model.Nombre, model.Apellido, model.Idioma, model.Pais));
                    TempData["success"] = "Asesor creadao correctamente";
                    return RedirectToAction("Index", new { idCategoria = model.IdCate, idConocimiento = model.IdConocimiento });

                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);
            }
        }



    }
    


    }
