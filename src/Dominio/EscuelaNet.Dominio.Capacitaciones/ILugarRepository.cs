﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EscuelaNet.Dominio.SeedWoork;

namespace EscuelaNet.Dominio.Capacitaciones
{
    public interface ILugarRepository : IRepository<Lugar>
    {
        Lugar Add(Lugar lugar);
        void Update(Lugar lugar);
        void Delete(Lugar lugar);
        Lugar GetLugar(int id);
        List<Lugar> ListLugar();
    }
}
