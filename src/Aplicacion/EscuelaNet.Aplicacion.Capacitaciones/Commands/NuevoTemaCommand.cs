﻿using EscuelaNet.Aplicacion.Capacitaciones.Responds;
using EscuelaNet.Dominio.Capacitaciones;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Aplicacion.Capacitaciones.Commands
{
    public class NuevoTemaCommand : IRequest<CommandRespond>
    {
        public string Nombre { get; set; }
        public NivelTema Nivel { get; set; }
    }
}
