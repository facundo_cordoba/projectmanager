﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Conocimientos
{
    public interface ICategoriaRepository : IRepository<Categoria>
    {
        Categoria Add(Categoria categoria);
        void Update(Categoria categoria);
        void Delete(Categoria categoria);
        Categoria GetCategoria(int id);
        List<Categoria> ListCategoria();
    }
}
