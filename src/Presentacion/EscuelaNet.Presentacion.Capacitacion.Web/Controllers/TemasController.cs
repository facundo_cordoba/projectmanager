﻿using EscuelaNet.Aplicacion.Capacitaciones.Commands;
using EscuelaNet.Aplicacion.Capacitaciones.QueryServices;
using EscuelaNet.Dominio.Capacitaciones;
using EscuelaNet.Infraestructura.Capacitaciones;
using EscuelaNet.Infraestructura.Capacitaciones.Repositorios;
using EscuelaNet.Presentacion.Capacitacion.Web.Infraestructura;
using EscuelaNet.Presentacion.Capacitacion.Web.Models;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Capacitacion.Web.Controllers
{
    public class TemasController : Controller
    {
        private ITemaRepository _temaRepositorio;
        private ITemasQuery _temaQuery;
        private IMediator _mediator;
        public TemasController(
            IMediator mediator,
            ITemaRepository temaRepositorio,
            ITemasQuery temaQuery)
        {
            _temaRepositorio = temaRepositorio;
            _temaQuery = temaQuery;
            _mediator = mediator;
        }

        // GET: Tema
        public ActionResult Index()
        {
            var temas = _temaQuery.ListTemas();

            var model = new TemaIndexModel()
            {
                Titulo = "Primera prueba",
                Temas = temas
            };
            return View(model);
        }

        // GET: Tema/New
        public ActionResult New()
        {
            var model = new NuevoTemaModel();
            return View(model);
        }
        // POST: Tema/New
        [HttpPost]
        public async Task<ActionResult> New(NuevoTemaCommand model)
        {
            var exito = await _mediator.Send(model);
            if (exito.Succes)
            {
                TempData["success"] = "Tema creado";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["error"] = exito.Error;
                var modelReturn = new NuevoTemaModel() {
                    Nombre = model.Nombre,
                    Nivel = model.Nivel
                };
                return View(modelReturn);
            }
        }



        // GET: Tema/Edit/5
        public ActionResult Edit(int id)
        {
            var tema = _temaQuery.GetTema(id);

            var model = new NuevoTemaModel()
            {
                Nombre = tema.Nombre,
                Id = id,
                Nivel = tema.Nivel
            };
            return View(model);
        }

        // POST: Tema2/Edit/5
        [HttpPost]
        public ActionResult Edit(NuevoTemaModel model)
        {
            if (!string.IsNullOrEmpty(model.Nombre))
            {
                try
                {
                    var tema = _temaRepositorio.GetTema(model.Id);
                    tema.Nombre = model.Nombre;
                    tema.Nivel = model.Nivel;

                    _temaRepositorio.Update(tema);
                    _temaRepositorio.UnitOfWork.SaveChanges();


                    TempData["success"] = "Tema Editado";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }
            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);

            }

        }

        // GET: Tema/Delete/5
        public ActionResult Delete(int id)
        {
            var tema = _temaQuery.GetTema(id);

            var model = new NuevoTemaModel()
            {
                Nombre = tema.Nombre,
                Id = id,
                Nivel = tema.Nivel,
            };
            return View(model);
        }

        // POST: Tema/Delete/5
        [HttpPost]
        public ActionResult Delete(NuevoTemaModel model)
        {
            try
            {
                var tema = _temaRepositorio.GetTema(model.Id);
                _temaRepositorio.Delete(tema);
                _temaRepositorio.UnitOfWork.SaveChanges();

                TempData["success"] = "Tema borrado";
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }
        }
    }
}
