﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EscuelaNet.Dominio.Capacitaciones;

namespace EscuelaNet.Presentacion.Capacitacion.Web.Models
{
    public class InstructorTemaModel
    {
        public Instructor Instructor { get; set; }
        public List<Tema> Temas { get; set; }
    }
}