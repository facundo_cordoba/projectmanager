﻿using Autofac;
using Autofac.Integration.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Clientes.Web.App_Start
{
    public class IoCConfigurator
    {
        public static void ConfigurarIoC()
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers();


        }
    }
}