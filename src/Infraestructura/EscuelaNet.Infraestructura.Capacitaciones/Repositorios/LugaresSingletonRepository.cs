﻿using EscuelaNet.Dominio.Capacitaciones;
using EscuelaNet.Dominio.SeedWoork;
using EscuelaNet.Infraestructura.Capacitacion.Singleton;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Infraestructura.Capacitaciones.Repositorios
{
    public class LugaresSingletonRepository : ILugarRepository
    {
        private CapacitacionSingleton contexto = CapacitacionSingleton.Instancia;
        public IUnitOfWork UnitOfWork => contexto;

        public Lugar Add(Lugar lugar)
        {
            contexto.Lugares.Add(lugar);
            return lugar;
        }

        public void Delete(Lugar lugar)
        {
            contexto.Lugares.Remove(lugar);
        }

        public Lugar GetLugar(int id)
        {
            return contexto.Lugares[id];
        }

        public List<Lugar> ListLugar()
        {
            return contexto.Lugares.ToList();
        }

        public void Update(Lugar lugar)
        {
            contexto.Lugares.RemoveAt(lugar.ID);
            contexto.Lugares.Insert(lugar.ID, lugar);
        }
    }
}
