﻿using EscuelaNet.Dominio.Proyectos;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Infraestructura.Proyectos.EntityTypeConfigurations
{
    public class ProyectoEntityTypeConfiguration : EntityTypeConfiguration<Proyecto>
    {
        public ProyectoEntityTypeConfiguration()
        {
            this.ToTable("Proyectos");
            this.HasKey<int>(proy => proy.ID);
            this.Property(proy => proy.ID).HasColumnName("IDProyecto");
            this.Property(proy => proy.Nombre).IsRequired();
            this.Property(proy => proy.Descripcion).IsRequired();
            this.Property(proy => proy.NombreResponsable).IsRequired();
            this.Property(proy => proy.EmailResponsable).IsRequired();
            this.Property(proy => proy.TelefonoResponsable).IsRequired();            
            //this.Ignore(proy => proy.Duracion);
            //this.Ignore<DateTime>(proy => proy.FechaFin);
            this.HasMany<Etapa>(proy => proy.Etapas)
                .WithRequired(etapa => etapa.Proyecto)
                .HasForeignKey<int>(etapa => etapa.IDProyecto);

        }
    }
}
