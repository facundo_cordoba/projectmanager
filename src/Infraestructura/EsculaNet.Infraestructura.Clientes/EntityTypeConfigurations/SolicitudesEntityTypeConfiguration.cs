﻿using EscuelaNet.Dominio.Clientes;
using System.Data.Entity.ModelConfiguration;

namespace EsculaNet.Infraestructura.Clientes.EntityTypeConfigurations
{
    public class SolicitudesEntityTypeConfiguration :
        EntityTypeConfiguration<Solicitud>
    {
        public SolicitudesEntityTypeConfiguration()
        {
            this.ToTable("Solicitudes");
            this.HasKey<int>(s => s.ID);
            this.Property(s => s.ID)
                .HasColumnName("IDSolicitud");
            this.Property(s => s.Titulo)                
                .IsRequired();
            this.Property(s => s.Descripcion)
                .IsRequired();
            this.Property(s => s.Estado)
               .IsRequired();

            this.HasMany<UnidadDeNegocio>(s => s.UnidadesDeNegocio)
                .WithMany(un => un.Solicitudes)
                .Map(us =>
                {
                    us.ToTable("UnidadSolicitud");                    
                    us.MapRightKey("IDUnidadDeNegocio");
                    us.MapLeftKey("IDSolicitud");
                });
        }
    }
}