﻿using EscuelaNet.Dominio.Clientes;
using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsculaNet.Infraestructura.Clientes.Repositorios
{
    public class SolicitudesRepository : ISolicitudRepository
    {
        private ClienteContext _contexto = new ClienteContext();
        public IUnitOfWork UnitOfWork => _contexto;

        public Solicitud Add(Solicitud solicitud)
        {
            _contexto.Solicitudes.Add(solicitud);
            return solicitud;
        }

        public void Update(Solicitud solicitud)
        {
            _contexto.Entry(solicitud).State =
                        EntityState.Modified;
        }

        public void Delete(Solicitud solicitud)
        {
            _contexto.Solicitudes.Remove(solicitud);
        }        

        public Solicitud GetSolicitud(int id)
        {
            var solicitud = _contexto.Solicitudes.Find(id);
            if (solicitud != null)
            {
                _contexto.Entry(solicitud)
                    .Collection(s => s.UnidadesDeNegocio).Load();
            }
            return solicitud;
        }

        public List<Solicitud> ListSolicitud()
        {
            return _contexto.Solicitudes.ToList();
        }

        public void addUnidad(Solicitud solicitud, UnidadDeNegocio unidad)
        {
            _contexto.Entry(solicitud).State =
                        EntityState.Unchanged;

            _contexto.Entry(unidad.Cliente).State =
                        EntityState.Unchanged;
            
            solicitud.pushUnidad(unidad);        

        }

        public void removeUnidad(Solicitud solicitud, UnidadDeNegocio unidad)
        {            
            try
            {
                solicitud.pullUnidad(unidad);
            }
            catch (Exception ex )
            {

                throw ex;
            }          

        }

    }
}
