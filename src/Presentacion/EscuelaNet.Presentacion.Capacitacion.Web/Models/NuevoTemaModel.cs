﻿using EscuelaNet.Dominio.Capacitaciones;

namespace EscuelaNet.Presentacion.Capacitacion.Web.Models
{
    public class NuevoTemaModel
    {
        public int Id { get; set; }
        public string Titulo { get; set; }
        public string Nombre { get; set; }
        public NivelTema Nivel { get; set; }
    }
}