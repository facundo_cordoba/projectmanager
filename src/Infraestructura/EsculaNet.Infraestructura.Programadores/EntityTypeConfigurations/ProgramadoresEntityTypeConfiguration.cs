﻿using EscuelaNet.Dominio.Programadores;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EsculaNet.Infraestructura.Programadores.EntityTypeConfigurations
{
    class ProgramadoresEntityTypeConfiguration : EntityTypeConfiguration<Programador>
    {
        public ProgramadoresEntityTypeConfiguration()
        {
            this.ToTable("Programador");
            this.HasKey<int>(p => p.ID);
            this.Property(p => p.ID)
                .HasColumnName("IdProgramador");
            this.Property(p => p.Nombre)
                .IsRequired();
            this.Property(p => p.Apellido)
                .IsRequired();
            this.Property(p => p.Legajo)
                .IsRequired();
            this.Property(p => p.Dni)
                .IsRequired();
            this.Property(p => p.Rol)
                .IsRequired();
            this.Property(p => p.FechaNacimiento)
                .IsRequired();
            this.Property(p => p.Disponibilidad)
                .IsRequired();
            this.Property(p => p.Equipo_ID)
                .IsRequired();
            this.HasMany<Skills>(p => p.Skills)
                .WithRequired(s => s.Programador)
                .HasForeignKey<int>(s => s.Programador_ID);
        }
    }
}
