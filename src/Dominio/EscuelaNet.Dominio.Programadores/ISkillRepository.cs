﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Programadores
{
    public interface ISkillRepository : IRepository<Skills>
    {
        Skills Add(Skills skill);
        void Update(Skills skill);
        void Delete(Skills skill);
        Skills GetSkill(int id);
        List<Skills> ListSkills();
    }
}
