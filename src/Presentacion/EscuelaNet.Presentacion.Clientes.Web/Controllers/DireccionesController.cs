﻿using EscuelaNet.Dominio.Clientes;
//using EscuelaNet.Presentacion.Clientes.Web.Infraestructura;
using EscuelaNet.Presentacion.Clientes.Web.Models;
using EsculaNet.Infraestructura.Clientes;
using EsculaNet.Infraestructura.Clientes.Repositorios;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EscuelaNet.Presentacion.Clientes.Web.Controllers
{
    public class DireccionesController : Controller
    {
        private IClienteRepository Repositorio = new ClientesRepository();
        public ActionResult Index(int id)
        {
            var unidadDeNegocio = Repositorio.GetUnidadDeNegocio(id);           
            if (unidadDeNegocio.Direcciones==null)
            {
                TempData["error"] = "Unidad sin direcciones";
                return RedirectToAction("../Unidades/Index/"+unidadDeNegocio.IDCliente);
            }
            var model = new DireccionesIndexModel()
            {
                Titulo = "Direcciones de la Unidad '"+unidadDeNegocio.RazonSocial+"' del Cliente '"+unidadDeNegocio.Cliente.RazonSocial+"'",
                Direcciones = unidadDeNegocio.Direcciones.ToList(),
                IdUnidad = id,
                IdCliente = unidadDeNegocio.IDCliente
            };

            return View(model);
        }

        public ActionResult New(int id)
        {
            var unidadDeNegocio = Repositorio.GetUnidadDeNegocio(id);
            var model = new NuevaDireccionModel()
            {
                Titulo = "Nueva Direccion para la  Unidad '"+unidadDeNegocio.RazonSocial+"'",
                IdUnidad = id
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult New(NuevaDireccionModel model)
        {
            if (!string.IsNullOrEmpty(model.Domicilio))
            {
                try
                {                    
                    var domicilio = model.Domicilio;
                    var localidad = model.Localidad;
                    var provincia = model.Provincia;
                    var pais = model.Pais;

                    var unidadDeNegocio = Repositorio.GetUnidadDeNegocio(model.IdUnidad);
                    var direccion = new Direccion(domicilio,localidad,provincia,pais);
                    
                    unidadDeNegocio.AgregarDireccion(direccion);
                    Repositorio.Update(unidadDeNegocio.Cliente);
                    Repositorio.UnitOfWork.SaveChanges();
                    TempData["success"] = "Dirección creada";
                    return RedirectToAction("Index/"+model.IdUnidad);

                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);
            }
        }
        
        public ActionResult Edit(int id)
        {

            if (id <= 0)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("../Clientes/Index");
            }
            else
            {
                var direccion = Repositorio.GetDireccion(id);
                var unidad = Repositorio.GetUnidadDeNegocio(direccion.IDUnidadDeNegocio);

                var model = new NuevaDireccionModel()
                {
                    Titulo="Editar una Direccion  de la Unidad'"+unidad.RazonSocial+"' del Cliente '"+ unidad.Cliente.RazonSocial+"'",
                    //IdCliente = direccion.Unidad.Cliente.ID,
                    IdUnidad = direccion.IDUnidadDeNegocio,
                    IdDireccion = id,
                    Domicilio = direccion.Domicilio,
                    Localidad = direccion.Localidad,
                    Provincia = direccion.Provincia,
                    Pais = direccion.Pais

                };

                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Edit(NuevaDireccionModel model)
        {
            if (!string.IsNullOrEmpty(model.Domicilio))
            {
                try
                {                    
                    var unidadDeNegocio = Repositorio.GetUnidadDeNegocio(model.IdUnidad);

                    unidadDeNegocio.Direcciones.Where(d => d.ID == model.IdDireccion).First().Domicilio = model.Domicilio;
                    unidadDeNegocio.Direcciones.Where(d => d.ID == model.IdDireccion).First().Localidad = model.Localidad;
                    unidadDeNegocio.Direcciones.Where(d => d.ID == model.IdDireccion).First().Provincia = model.Provincia;
                    unidadDeNegocio.Direcciones.Where(d => d.ID == model.IdDireccion).First().Pais = model.Pais;
                                        
                    Repositorio.Update(unidadDeNegocio.Cliente);
                    Repositorio.UnitOfWork.SaveChanges();
                    TempData["success"] = "Direccion editada";
                    return RedirectToAction("Index/"+model.IdUnidad);
                }
                catch (Exception ex)
                {
                    TempData["error"] = ex.Message;
                    return View(model);
                }

            }
            else
            {
                TempData["error"] = "Texto vacio";
                return View(model);
            }
        }

        public ActionResult Delete(int id)
        {
            if (id < 0)
            {
                TempData["error"] = "Id no valido";
                return RedirectToAction("../Clientes/Index");
            }
            else
            {
                var direccion = Repositorio.GetDireccion(id);
                var unidad = Repositorio.GetUnidadDeNegocio(direccion.IDUnidadDeNegocio);

                var model = new NuevaDireccionModel()
                {
                    Titulo = "Borrar una Direccion  de la Unidad'" + unidad.RazonSocial + "' del Cliente '" + unidad.Cliente.RazonSocial + "'",
                    //IdCliente = direccion.Unidad.Cliente.ID,
                    IdUnidad = direccion.IDUnidadDeNegocio,
                    IdDireccion = id,
                    Domicilio = direccion.Domicilio,
                    Localidad = direccion.Localidad,
                    Provincia = direccion.Provincia,
                    Pais = direccion.Pais

                };
                return View(model);
            }
        }

        [HttpPost]
        public ActionResult Delete(NuevaDireccionModel model)
        {

            try
            {
                var direccion = Repositorio.GetDireccion(model.IdDireccion);
                Repositorio.DeleteDireccion(direccion);
                Repositorio.UnitOfWork.SaveChanges();
                TempData["success"] = "Direccion borrada";
                return RedirectToAction("Index/" + model.IdUnidad);
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
                return View(model);
            }

        }

    }
}